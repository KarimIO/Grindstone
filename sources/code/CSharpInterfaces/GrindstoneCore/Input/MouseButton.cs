namespace Grindstone.Input {
	public enum MouseButton {
		Left,
		Middle,
		Right,

		Mouse4,
		Mouse5
	}
}
